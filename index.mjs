import { Client } from './salesforce-client.mjs'

let jsforceOptions
if (process.env.JSFORCE_OPTIONS) {
  try {
    jsforceOptions = JSON.parse(process.env.JSFORCE_OPTIONS)
  } catch (ex) {
    console.error('Cannot parse JSFORCE_OPTIONS', ex);
  }
}

if (!jsforceOptions) {
  console.error(`Please set JSFORCE_OPTIONS with the appropriate configuration options to establish salesforce connection. - looks like this now: '${(process.env.JSFORCE_OPTIONS || '').slice(0, 10)}'`)
  process.exit(-1)
}
if (process.argv.length !== 4) {
  console.error('Please set artifact_file_path and static_resource_name')
  process.exit(-1)
}

const [, , static_resource_name, artifact_file_path] = process.argv

const run = async () => {
  const client = new Client(jsforceOptions)

  try {
    await client.upload(static_resource_name, artifact_file_path)
  } catch ({ errorCode }) {
    if (errorCode === 'STORAGE_LIMIT_EXCEEDED') {
      console.log('trying to clean up and re-upload...')
      await client.cleanup()

      await client.upload(static_resource_name, artifact_file_path)
    }
  }
}
run()