import { readFile } from 'fs'
import { promisify } from 'util'

import jsforce from 'jsforce'

const readFileP = promisify(readFile)

export const AUTO_DESCRIPTION = 'Autodeployment of Static Resource from Bitrise'

export class Client {
  constructor(options) {
    this.conn = new jsforce.Connection(options)
  }
  async login(username, password) {
    await this.conn.login(username, password)
  }

  async upload(name, filename) {
    const fileContent = await readFileP(filename)
    const cleanName = name.replace(/[^a-zA-Z0-9]+/, '_')

    return this.conn.sobject('StaticResource').upsert({
      name,
      body: Buffer.from(fileContent).toString('base64'),
      contentType: 'application/zip',
      cacheControl: 'public',
      description: AUTO_DESCRIPTION
    }, 'name')
  }

  async cleanup(count = 2) {
    const isSandbox = this.conn.loginUrl.includes('test.salesforce.com')
    if (isSandbox) {
      const objectsToDelete = await this.conn.query(`SELECT Id FROM StaticResource WHERE Description = '${AUTO_DESCRIPTION}' ORDER BY LastModifiedDate ASC LIMIT ${count}`)
      const idsToDelete = objectsToDelete.records.map(({ Id }) => Id)

      console.log('Deleting', idsToDelete)
      await this.conn.sobject('StaticResource').delete(idsToDelete)
    }
  }
}